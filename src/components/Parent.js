import { Component } from "react";
import Display from "./Display";

class Parent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            displayChild: true
        }
    }

    updateDisplay = () => {
        this.setState({
            displayChild: false
        })
    }

    render() {
        return (
            <div>
                <button className="btn btn-info">Create Component</button>
                <div>{this.state.displayChild ? <Display display={this.updateDisplay} /> : null}</div>
            </div>
        )
    }
}

export default Parent;
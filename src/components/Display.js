import { Component } from "react";

class Display extends Component {
    constructor(props) {
        super(props)
        this.state = {
            destroy: false
        }
    }

    // componentDidUpdate() {
    //     console.log("Component Did Update");
    //     if (this.state.destroy) { //khi count = 10 xóa component đi
    //         this.props.display();
    //     }
    // }

    destroyButton = () => {
        this.setState({
            destroyButton: true
        })
    }

    render() {
        return (
            <div className="m-5">
                <h1 className="text-success"><em>I exist!</em></h1>
                <button className="btn btn-danger" onClick={this.destroyButton()}>Destroy Component</button>
            </div>
        )
    }
}

export default Display;
import Parent from "./components/Parent";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="container mt-5">
      <Parent />
    </div>
  );
}

export default App;
